# Introduction

This work is intended for political dissidents.
It aims to establish a theoretical framework for creating a private social and financial security mechanism for political dissidents.
At the same time, it also provides a strategy for creating a politically cohesive parallel society of dissidents.

**1. Dissident fates**&emsp;
In these times of enforced political correctness, it is hard to be a dissident.
Individuals are attacked by screeching mobs of activists if they not hold the mainstream "correct" opinion arranged by the globalist media.
Those who speak their "incorrect" beliefs are typically fired, deplatformed, or even physically harassed or abused.
The employers cannot really be blamed for firing their employees that have gotten under the fire of public outrage mobs, after all, they do not understand that it's a vocal, radical minority without much power that is threatening them.
For the sake of business, they want to have their company's reputation unblemished.
Individuals who are under attack by outrage mobs have usually no way to clear their name, as they are fighting an uphill battle.
<br />&emsp;
Thus, we can see that the current system makes it extremely risky to be a political dissident.
Dissidents are thus forced to either keep their mouth shut or even keep up a facade of being mainstream, which can be quite taxing psychically.

**2. The solution**&emsp;
Oversimplified, the solution is as follows: Fellow-minded dissidents band together to create businesses.
The primary goal of those businesses is not to send a political message, but to generate income.
Outwardly, the businesses present themselves as uncontroversial.
Inwardly, the businesses only consist of dissidents, which allows them to speak freely at work.
The groups of dissidents that work at a company have high trust and support each other financially in times of crisis.
This secures the existence of the dissidents in the long term.
Since only dissidents work in those companies, and the companies themselves are not controversial, they will not be targeted by outrage mobs.
<br />&emsp;
It is encouraged that dissident groups connect and merge, but they have to do so carefully.
This will create a growing community of like-minded dissidents.
They should marry inside their community, if possible, and have many children.
Children born into the community can be educated by the parents to become dissident themselves.
The result is a cohesive parallel society with high birthrates and strong trust.
