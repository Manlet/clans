# Clans

Since dissidents are mostly isolated from society, they have no strong familial communities to build on.
A clan is a family-like structure that dissidents form voluntarily.
Clans have an internal hierarchy of command.
A clan's hierarchy of command allows coordinating the efforts of it's members to further its prosperity.
Entering a clan demands commitment and subordination to its goals.

**Goals**&emsp;
The first goal of a clan is to create and run businesses to make money, which is used as financial security fall-back mechanism for the clan members.
The second goal is to create a community where dissidents can be at ease and come together with like-minded individuals.
The third goal is to simplify finding a trustworthy and like-minded partner.
The fourth goal is to increase its numbers via recruiting and procreation to strengthen the political impact the dissidents have.

## 1. Clan founding
Clans are founded by like-minded dissidents who know each other well and trust each other.
Clans are assumed to have very few initial members, because dissidents are usually a minority.
The existence of the clan is not to be spread to outsiders, so it has no name, symbols, or similar things.
The founding members are the initial clan leaders.

## 2. Clan operation
**Residences**&emsp;
While clans are communities, they do not have a central residence in which members live.
Instead, the members live in normal houses or apartments.
This makes it so that the clan is not recognizable as an entity from the outside.
This ensures that nothing like the Waco siege can happen to it.
*can a clan provide housing to its members (and others) as a business?*

**Businesses**&emsp;
A clan should own at least one business.
Clan businesses should employ only clan members, if possible.
Non-clan employees threaten the clan members' ability to speak their mind at work without the fear of ostracization.
In the spirit common to start-ups, members help with the initial funding of the company, and every member receives shares of the clan's businesses, according to his standing in the clan.
This makes it so that the clan leaders have the greatest shares of all the clan's businesses and can legally coordinate all the clan's businesses.

**Schooling**&emsp;
Adult members of the clans are assumed to be convinced of and steadfast in their political beliefs.
Children born into the clan must become members and be schooled by their parents or other clan members.
For this purpose, there should be a curated set of lectures that can be used as school books.
Not every clan has to create their own curated list of school books; instead, the materials can be created anonymously in a collaborative effort on-line.
This allows dissidents from all over the world that do not necessarily trust each other to standardise and improve teaching materials.
These materials must show the lies taught by the system and offer the actual truth.
They must not contain lies, because that will weaken the child's trust in its parents, and make it believe the system, possibly exposing the clan, which would be fatal.
Starting at a young age, the child must be properly instructed and prepared to set up a facade at school, and not tell anyone about the things taught at home.

**Identity**&emsp;
While the existence of the clan must not be made public, it is important that members strongly identify with the clan and have a strong sense of camaraderie.
This can be achieved by giving a clan a name, and a banner/insignia which are used in in places where clan members gather.
The clan should also strive to have frequent social get-togethers, which involve group activities such as feasts or excursions.
The clan members and their children belong to the clan "family", and identify with the clan name.
However, since the clan is not public, they cannot have the clan name in their official name.
When clans merge, either a new clan identity is created, or both old identities prevail, and both clans enter an alliance.
To prevent too much centralisation, clans should not merge too often, because the clan leaders will have too much power and become a central point of failure.
Thus, in the long term, larger clans should form a network of alliances instead of one large clan.

**Marriage**&emsp;
Marriage within the clan should be strongly preferred to marriage outside the clan.
The holy institution of marriage has to be respected, and a divorce is not acceptable.
The benefit of marriage within the clan is that within few generations, the clan is bound by bloodline relations.
It will ensure the continued existence of the clan and preserve the high trust between members.
